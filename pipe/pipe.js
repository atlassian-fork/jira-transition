const _ = require('lodash')
const debug = require('debug')('pipe')

module.exports = class {
  constructor ({ Jira }) {
    this.Jira = Jira
    this.context = {
      issue: {},
      myself: {},
    }
  }

  async execute (params) {
      return this.executeWrapped(params)
  }

  async executeWrapped ({ transitionName, issueKeyString }) {
    const myself = await this.Jira.getMyself()
      .catch((error) => {
        console.error('Cannot authenticate with Jira. Please check credentials')

        throw error
      })

    this.context.myself = myself

    const issue = await this.extractIssueFrom(issueKeyString)
      .catch((error) => {
        console.error('Failed to retrieve issue from Jira')

        throw error
      })

    if (!issue) {
      throw new Error('Cannot find issue key')
    }

    this.context.issue = issue

    const { transitions } = await this.Jira.getIssueTransitions(issue.key)
      .catch((error) => {
        console.error('Failed to retrieve transitions from Jira')

        throw error
      })

    const transitionToApply = _.find(transitions, (t) => {
      if (transitionName.toLowerCase() === t.name.toLowerCase()) return true
    })

    if (!transitionToApply) {
      console.log('Please specify transition name or transition id.')
      console.log('Possible transitions:')
      transitions.forEach((t) => {
        console.log(`{ id: ${t.id}, name: ${t.name} } transitions issue to '${t.to.name}' status.`)
      })

      throw new Error('No transition to apply')
    }

    debug(`Selected transition:${JSON.stringify(transitionToApply, null, 4)}`)

    await this.Jira.transitionIssue(issue.key, {
      transition: {
        id: transitionToApply.id,
      },
    }).catch((error) => {
      console.log('Failed to perform transition')

      throw error
    })

    console.log(`Changed ${issue.key} status to : ${_.get(transitionToApply, 'to.name')} .`)
    console.log(`Link to issue: ${this.Jira.baseUrl}/browse/${issue.key}`)
  }

  async extractIssueFrom (extractString) {
    const issueIdRegEx = /([a-zA-Z0-9]+-[0-9]+)/g

    const match = extractString.match(issueIdRegEx)

    if (!match) {
      console.log(`String "${extractString}" does not contain issueKeys`)

      return
    }

    for (const issueKey of match) {
      debug(`searching for issue ${issueKey} ...`)
      const issue = await this.Jira.getIssue(issueKey)

      if (issue) {
        debug(`found issue ${issueKey}: ${JSON.stringify(issue, null, 4)}`)

        return issue
      }
    }
  }
}
